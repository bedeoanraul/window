import escape from "css.escape";
import { isArray, isObject, isString } from "lodash";

class DotStyles {
  constructor(config) {
    this.debug = () => {};
    this.config = config;
    this.registry = {};
    this.utilities = {
      static: {},
      dynamic: []
    };
  }
  createStyleSheets() {
    const normalStyles = document.createElement("style");
    normalStyles.id = "dot-styles-normal";
    normalStyles.type = "text/css";

    const mediaStyles = document.createElement("style");
    mediaStyles.id = "dot-styles-media";
    mediaStyles.type = "text/css";

    const head = document.getElementsByTagName("body")[0];
    head.appendChild(normalStyles);
    head.appendChild(mediaStyles);

    this.styleSheets = {
      normal: normalStyles.sheet,
      media: mediaStyles.sheet
    };
  }
  addUtility(a, b) {
    const utility = new DotUtility(this, a, b);

    if (utility.name) {
      this.utilities.static[utility.name] = utility;
    } else {
      this.utilities.dynamic.push(utility);
    }

    return utility;
  }
  addRawUtility(a, b) {
    const utility = this.addUtility(a, b);

    utility.setRaw();

    return utility;
  }
  splitName(name) {
    const [a, media] = name.split("@", 2);
    const [base, ...states] = a.split(":");

    return { base, states, media };
  }
  makeDot(name) {
    if (!name) return false;
    if (this.registry[name]) return false;

    this.registry[name] = true;

    const { base, states, media } = this.splitName(name);

    if (this.utilities.static[base]) {
      this.utilities.static[base].makeDot({ name, base, states, media });
    }

    this.utilities.dynamic.forEach(utility => {
      utility.makeDot({ name, base, states, media });
    });
  }
  run(input) {
    let r;

    if (isString(input)) {
      r = input.split(/\s+/);
      r.forEach(name => this.makeDot(name));
    } else if (isArray(input)) {
      r = input.map(item => this.dot(item));
    } else if (isObject(input)) {
      r = Object.keys(input)
        .filter(key => input[key])
        .map(key => this.dot(key));
    }

    return r ? r.filter(name => name).join(" ") : "";
  }
}

class DotUtility {
  constructor(dot, a, b) {
    this.dot = dot;

    if (a instanceof RegExp) {
      this.pattern = a;
    } else {
      this.name = a;
    }

    if (b instanceof Function) {
      this.resolver = b;
    } else {
      this.declarations = b;
    }
  }
  setRaw() {
    this.raw = true;
  }
  makeDot({ name, base, states, media }) {
    let args = [],
      declarations;

    if (this.pattern) {
      const match = base.match(this.pattern);
      if (match) {
        args = match.slice(1);
      } else {
        return false;
      }
    }

    if (this.resolver) {
      declarations = this.makeDeclarations(this.resolver(...args));
    } else {
      declarations = this.makeDeclarations(this.declarations);
    }

    if (this.raw) {
      this.createRawStyleRule(declarations);
    } else {
      this.createStyleRule({ name, media, states, declarations });
    }
  }
  makeDeclarations(input) {
    if (isString(input)) return input;
    if (isArray(input)) return input.join(";");
    if (isObject(input))
      return Object.keys(input)
        .map(key => key + ":" + input[key])
        .join(";");
  }
  createRawStyleRule(str) {
    this.dot.debug("createRawStyleRule", str);

    this.dot.styleSheets.normal.insertRule(str);
  }
  createStyleRule({ name, states, media, declarations }) {
    let str = `.${escape(name)}`;

    if (states.indexOf("group-hover") !== -1) {
      str = `.group:hover ${str}`;
      states = states.filter(state => state !== "group-hover");
    }

    if (states.length) {
      str = `${str}:${states.join(":")}`;
    }

    str = `${str} { ${declarations} }`;

    if (media && this.dot.config.screens[media]) {
      str = `@media (min-width: ${this.dot.config.screens[media]}) { ${str} }`;
    }

    this.dot.debug("createStyleRule", str);

    if (media) {
      this.dot.styleSheets.media.insertRule(str);
    } else {
      this.dot.styleSheets.normal.insertRule(str);
    }
  }
}

export default DotStyles;
