export function bindRouterQuery(name, defaultValue = null) {
  return {
    get() {
      return this.$route.query[name] || defaultValue;
    },
    set(value) {
      const newValue = value === defaultValue ? null : value;
      const oldQuery = this.$route.query;
      const newQuery = { ...oldQuery, [name]: newValue || undefined };
      this.$router.push({ query: newQuery });
    }
  };
}
