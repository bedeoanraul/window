import firebase from "firebase/app";
import "firebase/auth";
import "firebase/storage";
import "firebase/firestore";
// import "firebase/database";
// import "firebase/messaging";
// import "firebase/functions";

firebase.initializeApp({
  apiKey: "AIzaSyAWgTdZ5Z8WXehvr2Z0IZNEVWgCquwD_RU",
  authDomain: "window-90b16.firebaseapp.com",
  databaseURL: "https://window-90b16.firebaseio.com",
  projectId: "window-90b16",
  storageBucket: "window-90b16.appspot.com",
  messagingSenderId: "275720099032",
  appId: "1:275720099032:web:d0940f213b920ade"
});

firebase.firestore().settings({
  timestampsInSnapshots: true
});

if (process.env.NODE_ENV === "development") {
  // firebase.firestore.setLogLevel("debug");
}

export default firebase;
