import Vue from "vue";
import Router from "vue-router";
// import Home from "@/views/Home.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      redirect: "/projects"
      // component: Home
    },
    {
      path: "/materials",
      component: () =>
        import(/* webpackChunkName: "materials" */ "@/views/Materials.vue")
    },
    {
      path: "/materials/categories",
      component: () =>
        import(/* webpackChunkName: "material-categories" */ "@/views/MaterialCategories.vue")
    },
    {
      path: "/projects",
      component: () =>
        import(/* webpackChunkName: "projects" */ "@/views/Projects.vue")
    },
    {
      path: "/projects/:id",
      props: true,
      component: () =>
        import(/* webpackChunkName: "project-details" */ "@/views/ProjectDetails.vue")
    },
    {
      path: "/tasks",
      component: () =>
        import(/* webpackChunkName: "tasks" */ "@/views/Tasks.vue")
    },
    {
      path: "/tasks/categories",
      component: () =>
        import(/* webpackChunkName: "task-categories" */ "@/views/TaskCategories.vue")
    },
    {
      path: "/clients",
      component: () =>
        import(/* webpackChunkName: "clients" */ "@/views/Clients.vue")
    },
    {
      path: "/suppliers",
      component: () =>
        import(/* webpackChunkName: "suppliers" */ "@/views/Suppliers.vue")
    },
    {
      path: "/activities",
      component: () =>
        import(/* webpackChunkName: "activities" */ "@/views/Activities.vue")
    },
    {
      path: "/settings",
      component: () =>
        import(/* webpackChunkName: "settings" */ "@/views/Settings.vue")
    },
    {
      path: "/services",
      component: () =>
        import(/* webpackChunkName: "services" */ "@/views/Services.vue")
    },
    {
      path: "/calculator",
      component: () =>
        import(/* webpackChunkName: "calculator" */ "@/views/Calculator.vue")
    },
    {
      path: "/tinker",
      component: () =>
        import(/* webpackChunkName: "tinker" */ "@/views/Tinker.vue")
    }
  ]
});
