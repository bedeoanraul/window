export const STATE_PLAN = "plan";
export const STATE_WORK = "work";
export const STATE_DONE = "done";

export default {
  data() {
    return {
      user: null,
      userLoading: true,
      projectStates: {
        [STATE_PLAN]: "Planificare",
        [STATE_WORK]: "In Lucru",
        [STATE_DONE]: "Finalizat"
      }
    };
  },
  methods: {
    addProjectActivity(data) {
      this.$firestore()
        .collection("activities")
        .add({
          ...data,
          createdAt: this.$firestore.FieldValue.serverTimestamp()
        });
    }
  }
};
