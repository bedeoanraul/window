import DotStyles from "@/utils/DotStyles";

const dot = new DotStyles({
  screens: {
    sm: "576px",
    md: "768px",
    lg: "992px",
    xl: "1200px"
  }
});

dot.addUtility("test1", "background: red");
dot.addUtility("test2", () => "background: red");
dot.addUtility(/^test3$/, "background: red");
dot.addUtility(/^test4$/, () => "background: red");
dot.addRawUtility("test5", ".test5 { background: red }");

// spacing
dot.addUtility(/^([mp])([axytrbl]{0,1})-([0-9]+)$/, (type, side, size) => {
  const declarations = {};
  const property = { m: "margin", p: "padding" }[type];
  const value = `${parseInt(size) * 4}px`;

  if (side === "" || side === "a") {
    declarations[property] = value;
  }
  if (side === "l" || side === "x") {
    declarations[`${property}-left`] = value;
  }
  if (side === "r" || side === "x") {
    declarations[`${property}-right`] = value;
  }
  if (side === "t" || side === "y") {
    declarations[`${property}-top`] = value;
  }
  if (side === "b" || side === "y") {
    declarations[`${property}-bottom`] = value;
  }

  return declarations;
});

// flex
dot.addUtility("flex", "display: flex");
dot.addUtility("flex-row", "flex-direction: row");
dot.addUtility("flex-col", "flex-direction: column");
dot.addUtility("flex-grow", "flex-grow: 1");

dot.createStyleSheets();
// dot.debug = console.log;

export default dot.run.bind(dot);
