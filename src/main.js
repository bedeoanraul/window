import Vue from "vue";

import App from "@/App";
import dot from "@/services/dot";
import store from "@/services/store";
import router from "@/services/router";
import firebase from "@/services/firebase";
import VueFirestore from "@/utils/VueFirestore";

import "@/style.css";
import "@/components";
import "@/plugins/element";
import "@/registerServiceWorker";

Vue.config.productionTip = false;

Vue.use(VueFirestore);

Vue.prototype.$dot = dot;
Vue.prototype.$auth = firebase.auth;
Vue.prototype.$store = new Vue(store);
Vue.prototype.$storage = firebase.storage;
Vue.prototype.$firestore = firebase.firestore;

new Vue({
  router,
  mixins: [store],
  render: h => h(App)
}).$mount("#app");
