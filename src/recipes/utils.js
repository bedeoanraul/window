import { omitBy, sumBy, mapValues, maxBy, flatMap } from "lodash";

function sheetFitsPrinter(sheet, printer) {
  return (
    (sheet.width >= printer.width.min &&
      sheet.width <= printer.width.max &&
      sheet.height >= printer.height.min &&
      sheet.height <= printer.height.max) ||
    (sheet.width >= printer.height.min &&
      sheet.width <= printer.height.max &&
      sheet.height >= printer.width.min &&
      sheet.height <= printer.width.max)
  );
}

function getLargeSheetLayouts(largeSheet, printer) {
  const printerMin = Math.min(printer.width.min, printer.height.min);
  const printerMax = Math.max(printer.width.max, printer.height.max);

  const xMin = Math.ceil(largeSheet.width / printerMax);
  const xMax = Math.floor(largeSheet.width / printerMin);
  const yMin = Math.ceil(largeSheet.height / printerMax);
  const yMax = Math.floor(largeSheet.height / printerMin);

  const result = [];
  let x, y;

  for (x = xMin; x <= xMax; x++) {
    for (y = yMin; y <= yMax; y++) {
      const printSheet = {
        width: largeSheet.width / x,
        height: largeSheet.height / y
      };
      if (sheetFitsPrinter(printSheet, printer)) {
        const largeSheetLayout = { x, y };
        result.push({ largeSheetLayout, printSheet });
      }
    }
  }

  return result;
}

function getPrintSheetSafeSize(printSheet, printer) {
  const { margins } = printer;
  return {
    safeWidth: printSheet.width - margins.left - margins.right,
    safeHeight: printSheet.height - margins.top - margins.bottom
  };
}

function getPrintSheetDefaultLayout(printSheet, finalSheet, printer) {
  const { safeWidth, safeHeight } = getPrintSheetSafeSize(printSheet, printer);
  const x = Math.floor(safeWidth / finalSheet.width);
  const y = Math.floor(safeHeight / finalSheet.height);
  const coverageX = (finalSheet.width * x) / printSheet.width;
  const coverageY = (finalSheet.height * y) / printSheet.height;

  return {
    printSheetLayout: { x, y, type: "default" },
    coverage: coverageX * coverageY
  };
}

function getPrintSheetTurnableLayout(printSheet, finalSheet, printer) {
  const { safeWidth, safeHeight } = getPrintSheetSafeSize(printSheet, printer);
  let x = Math.floor(safeWidth / finalSheet.width);
  let y = Math.floor(safeHeight / finalSheet.height);

  if (x % 2 === 1 && y % 2 === 1) {
    if (x > y) {
      x--;
    } else {
      y--;
    }
  }

  const coverageX = (finalSheet.width * x) / printSheet.width;
  const coverageY = (finalSheet.height * y) / printSheet.height;

  return {
    printSheetLayout: { x, y, type: "turnable" },
    coverage: coverageX * coverageY
  };
}

function getPrintSheetFoldableLayout(printSheet, finalSheet, printer) {
  const { safeWidth, safeHeight } = getPrintSheetSafeSize(printSheet, printer);
  let x = 0;
  let y = 0;
  let nextX = 2;
  let nextY = 1;

  while (
    finalSheet.width * nextX <= safeWidth &&
    finalSheet.height * nextY <= safeHeight
  ) {
    x = nextX;
    y = nextY;
    if (nextX === nextY) {
      nextX *= 2;
    } else {
      nextY *= 2;
    }
  }

  const coverageX = (finalSheet.width * x) / printSheet.width;
  const coverageY = (finalSheet.height * y) / printSheet.height;

  return {
    printSheetLayout: { x, y, type: "foldable" },
    coverage: coverageX * coverageY
  };
}

function getLayouts(largeSheet, finalSheet, printer, type) {
  const getPrintSheetLayout = {
    default: getPrintSheetDefaultLayout,
    turnable: getPrintSheetTurnableLayout,
    foldable: getPrintSheetFoldableLayout
  }[type];
  return getLargeSheetLayouts(largeSheet, printer).map(
    ({ largeSheetLayout, printSheet }) => {
      const { printSheetLayout, coverage } = getPrintSheetLayout(
        printSheet,
        finalSheet,
        printer
      );
      return {
        finalSheet,
        printSheet,
        largeSheet,
        printSheetLayout,
        largeSheetLayout,
        coverage
      };
    }
  );
}

export function autoLayout({
  largeSheets,
  finalSheet,
  printer,
  type = "default"
}) {
  const layouts = flatMap(largeSheets, largeSheet =>
    getLayouts(largeSheet, finalSheet, printer, type)
  );

  const bestLayout = maxBy(layouts, "coverage");
  if (!bestLayout) return null;

  const printSheetsOnLargeSheet =
    bestLayout.largeSheetLayout.x * bestLayout.largeSheetLayout.y;
  const finalSheetsOnPrintSheet =
    bestLayout.printSheetLayout.x * bestLayout.printSheetLayout.y;
  const finalSheetsOnLargeSheet =
    printSheetsOnLargeSheet * finalSheetsOnPrintSheet;

  return {
    ...bestLayout,
    printSheetsOnLargeSheet,
    finalSheetsOnPrintSheet,
    finalSheetsOnLargeSheet
  };
}

//

export function getPageGroups({ pages, pagesOnSheet }) {
  let group = Math.min(pages, pagesOnSheet);
  const groups = [];
  while (pages) {
    if (pages >= group) {
      pages -= group;
      groups.push(group);
    } else {
      group = Math.floor(group / 2);
    }
  }
  return groups;
}

export function groupSheets(total, max) {
  let group = max;
  const groups = [];
  while (total) {
    if (total >= group) {
      total -= group;
      groups.push(group);
    } else {
      group = Math.floor(group / 2);
    }
  }
  return groups;
}

//

export function priceRecipe(
  recipe,
  finalQuantity,
  materialsById,
  servicesById
) {
  const pricedComponents = recipe.components.map(component =>
    priceComponent(component, finalQuantity, materialsById, servicesById)
  );
  const totalPrice = sumBy(pricedComponents, "price");
  const unitPrice = totalPrice / finalQuantity;

  return {
    ...recipe,
    quantity: finalQuantity,
    components: pricedComponents,
    totalPrice,
    unitPrice
  };
}

function priceComponent(component, finalQuantity, materialsById, servicesById) {
  const baseQuantity = component.value * finalQuantity;
  const surplusQuantity = component.surplus
    ? scale(baseQuantity, component.surplus)
    : 0;
  const componentQuantity = baseQuantity + surplusQuantity;

  const pricedMaterials = mapValues(component.materials, material =>
    priceMaterial(material, componentQuantity, materialsById)
  );
  const pricedServices = mapValues(component.services, service =>
    priceService(service, componentQuantity, servicesById)
  );

  const componentPrice =
    sumBy(Object.values(pricedMaterials), "price") +
    sumBy(Object.values(pricedServices), "price");

  return {
    ...component,
    baseQuantity,
    surplusQuantity,
    quantity: componentQuantity,
    materials: pricedMaterials,
    services: pricedServices,
    price: componentPrice
  };
}

function priceMaterial(material, componentQuantity, materialsById) {
  const multiplier = material.fixed ? 1 : componentQuantity;
  const materialQuantity = material.value * multiplier;
  const materialPrice =
    materialQuantity * materialsById[material.id].data.price;

  return {
    ...material,
    quantity: materialQuantity,
    price: materialPrice
  };
}

function priceService(service, componentQuantity, servicesById) {
  const multiplier = service.fixed ? 1 : componentQuantity;
  const serviceQuantity = service.value * multiplier;
  const servicePrice = scale(
    serviceQuantity,
    servicesById[service.id].data.price
  );

  return {
    ...service,
    quantity: serviceQuantity,
    price: servicePrice
  };
}

function scale(value, slope) {
  const { base, rise, unit } = slope;

  return base + (value / unit) * rise;
}

//

export function omitFalseValues(object) {
  return omitBy(object, value => value === false);
}

export const PLATE_MATERIAL = "ZdNAB3T7Lv28hiH9jbeG";
export const PLATE_SERVICE = "r1e2eMCl4tsbD3f5PksB";
export const VARNISH_SERVICE = "uqvRCKIfCMC381hHz69o";
export const LAMINATION_SERVICE = "XAVL2y9mh39ajpYnsOwX";
export const CREASING_SERVICE = "MwNZjEvJabK9Zfxb9QTY";
export const CHECKING_SERVICE = "OOO9KAML9VZ6lCFchTYO";
export const PACKING_SERVICE = "gpi8e8McNIuEvfLsZVwt";
export const SHIPPING_SERVICE = "VweAPRDgHd6TZFHE79x1";
export const WIRE_BINDING_SERVICE = "pLeD3BZf4mNzFy00qSnc";
export const SADDLE_BINDING_SERVICE = "DXPiw1c0GaT3eMafx6v1";
export const PERFECT_BINDING_SERVICE = "3NVxRNrMiW8Y4KBsjB8p";
export const BOOKLET_CUTTING_SERVICE = "IG7wxXGVBT0LMHj343U5";
export const BOOK_CUTTING_SERVICE = "sw2lW8DUiyPaEJXn0pgE";
export const DIE_CUTTING_SERVICE = "K50WBfUeqWblrYLBXC2d";
export const COLLATION_SERVICE = "CWl1sgueZgWW5ARgG3yA";
export const GLUING_SERVICE = "ZkuUHl8ptWsTXuIXhpKi";

export function getPrintingService(colors) {
  const printingServiceIds = [
    "hmcGDyqEEhsdZb1eln7E",
    "HXuvy1SkdpV6EtUUpDrC",
    "rqzKJYFwVDdhiXve5FEq",
    "C8dErqAyDEJp4dIzOurj",
    "EZeItprwFJf0j4ObcUyn"
  ];
  return printingServiceIds[colors - 1];
}

export function getCuttingService(items) {
  const cuttingServiceIds = [
    "eo0lWBhxPFOb7LUvKa5f",
    "irkn93rqwh3iPcZKBSK9",
    "fj8vietqnmPrlDz5d2l6",
    "A0h61Ig7KCph1ApAM0qK",
    "REG6myaGXvEbMxsrDeVF"
  ];
  const group = Math.min(Math.ceil(items / 4), 5);
  return cuttingServiceIds[group - 1];
}

export function getLinearFoldingService(pages) {
  const foldingServiceByPages = {
    4: "k7O61axUHb59WlvHZCcV",
    6: "wLIIlS7q94OYRf3YlVLx"
  };
  return foldingServiceByPages[pages];
}

export function getSignatureFoldingService(pages) {
  const foldingServiceByPages = {
    4: "K9ewdaqxk5ROKw6VL5b3",
    8: "ZRH1dyHYz3i4J6lOCRua",
    16: "pM62uE6Q9jy5KdQggHJQ",
    32: "A2aBhomuFo6pvnTbmXJ5"
  };
  return foldingServiceByPages[pages];
}

//

function getPlates(finalSheetsOnPrintSheet, frontAndBack) {
  if (frontAndBack === false) return 1;
  return finalSheetsOnPrintSheet % 2 === 1 ? 2 : 1;
}

export const generalComponent = {
  name: "General",
  value: 1,
  surplus: { base: 0, rise: 0, unit: 1 },
  services: {
    verificare: {
      id: CHECKING_SERVICE,
      value: 1,
      fixed: true
    },
    ambalare: {
      id: PACKING_SERVICE,
      value: 1,
      fixed: true
    },
    livrare: {
      id: SHIPPING_SERVICE,
      value: 1,
      fixed: true
    }
  }
};

export function createOffsetComponent({
  layout,
  colors,
  frontAndBack,
  sheetsInGroup
}) {
  const plates = getPlates(
    layout.finalSheetsOnPrintSheet / sheetsInGroup,
    frontAndBack
  );

  return {
    name: "Unnamed",
    value: sheetsInGroup / layout.finalSheetsOnPrintSheet,
    surplus: { base: 50, rise: 10, unit: 1000 },
    materials: {
      matrite: {
        id: PLATE_MATERIAL,
        value: plates * colors,
        fixed: true
      },
      coliMari: {
        id: layout.largeSheet.materialId,
        value: 1 / layout.printSheetsOnLargeSheet
      }
    },
    services: {
      matrite: {
        id: PLATE_SERVICE,
        value: plates * colors,
        fixed: true
      },
      tipar: {
        id: getPrintingService(colors),
        value: frontAndBack ? 2 : 1
      },
      formatizare1: {
        id: getCuttingService(layout.printSheetsOnLargeSheet),
        value: 1 / layout.printSheetsOnLargeSheet
      },
      formatizare2: {
        id: getCuttingService(layout.finalSheetsOnPrintSheet / sheetsInGroup),
        value: 1
      }
    }
  };
}
