import {
  autoLayout,
  omitFalseValues,
  VARNISH_SERVICE,
  createOffsetComponent
} from "@/recipes/utils";

export function getOptions() {
  return {
    quantity: 1000,
    width: 15,
    height: 21,
    back: true,
    weight: 80,
    colors: 4,
    varnish: false
  };
}

export function getRecipe(options, printer, sheets) {
  const largeSheets = sheets
    .filter(sheet => sheet.data.weight === options.weight)
    .map(material => ({
      materialId: material.id,
      width: material.data.width,
      height: material.data.height
    }));
  const finalSheet = {
    width: options.width,
    height: options.height
  };
  const layout = autoLayout({
    largeSheets,
    finalSheet,
    printer,
    type: options.frontAndBack ? "turnable" : "default"
  });
  const base = createOffsetComponent({
    layout,
    colors: options.colors,
    frontAndBack: options.frontAndBack,
    sheetsInGroup: 1
  });

  return {
    layouts: [layout],
    components: [
      {
        ...base,
        name: "Flyer",
        services: omitFalseValues({
          ...base.services,
          lacuire: options.varnish && {
            id: VARNISH_SERVICE,
            value: 1
          }
        })
      }
    ]
  };
}
