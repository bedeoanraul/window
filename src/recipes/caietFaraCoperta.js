import {
  groupSheets,
  autoLayout,
  COMBO_SERVICE,
  createOffsetComponent
} from "@/recipes/utils";

export function getOptions() {
  return {
    quantity: 1000,
    width: 15,
    height: 21,
    weight: 80,
    pages: 24,
    colors: 1
  };
}

export function getRecipe(options, printer, sheets) {
  const largeSheets = sheets
    .filter(sheet => sheet.data.weight === options.weight)
    .map(material => ({
      materialId: material.id,
      width: material.data.width,
      height: material.data.height
    }));
  const finalSheet = {
    width: options.width,
    height: options.height
  };
  const layout = autoLayout({
    largeSheets,
    finalSheet,
    printer,
    type: "foldable"
  });
  const groups = groupSheets(options.pages / 2, layout.finalSheetsOnPrintSheet);

  return {
    layouts: [layout],
    components: [
      ...groups.map((group, index) => {
        const base = createOffsetComponent({
          layout,
          colors: options.colors,
          frontAndBack: true,
          sheetsInGroup: group
        });
        return {
          ...base,
          name: `Coala #${index + 1}`
        };
      }),
      {
        name: "Postpress",
        value: 1,
        materials: {},
        services: {
          post: {
            id: COMBO_SERVICE,
            value: 1
          }
        }
      }
    ]
  };
}
