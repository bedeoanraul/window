import {
  autoLayout,
  omitFalseValues,
  VARNISH_SERVICE,
  getFoldingService,
  createOffsetComponent
} from "@/recipes/utils";

export function getOptions() {
  return {
    quantity: 1000,
    width: 30,
    height: 21,
    weight: 80,
    pages: 6,
    colors: 4,
    varnish: false
  };
}

export function getRecipe(options, printer, sheets) {
  const largeSheets = sheets
    .filter(sheet => sheet.data.weight === options.weight)
    .map(material => ({
      materialId: material.id,
      width: material.data.width,
      height: material.data.height
    }));
  const finalSheet = {
    width: options.width,
    height: options.height
  };
  const layout = autoLayout({
    largeSheets,
    finalSheet,
    printer,
    type: "turnable"
  });
  const base = createOffsetComponent({
    layout,
    colors: options.colors,
    frontAndBack: true,
    sheetsInGroup: 1
  });

  return {
    layouts: [layout],
    components: [
      {
        ...base,
        name: "Pliant!",
        services: omitFalseValues({
          ...base.services,
          lacuire: options.varnish && {
            id: VARNISH_SERVICE,
            value: 1
          },
          faltuireBiguire: {
            id: getFoldingService(options.pages),
            value: layout.finalSheetsOnPrintSheet
          }
        })
      }
    ]
  };
}
