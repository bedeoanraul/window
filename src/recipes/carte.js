import {
  groupSheets,
  autoLayout,
  omitFalseValues,
  VARNISH_SERVICE,
  createOffsetComponent
} from "@/recipes/utils";

export function getOptions() {
  return {
    quantity: 1000,
    width: 15,
    height: 21,
    pages: 100,
    interior: {
      weight: 80,
      colors: 1
    },
    cover: {
      weight: 80,
      colors: 4,
      varnish: false
    }
  };
}

export function getRecipe(options, printer, sheets) {
  const interiorLargeSheets = sheets
    .filter(sheet => sheet.data.weight === options.interior.weight)
    .map(material => ({
      materialId: material.id,
      width: material.data.width,
      height: material.data.height
    }));
  const interiorFinalSheet = {
    width: options.width,
    height: options.height
  };
  const interiorLayout = autoLayout({
    largeSheets: interiorLargeSheets,
    finalSheet: interiorFinalSheet,
    printer,
    type: "foldable"
  });
  const groups = groupSheets(
    options.pages / 2,
    interiorLayout.finalSheetsOnPrintSheet
  );
  const coverLargeSheets = sheets
    .filter(sheet => sheet.data.weight === options.cover.weight)
    .map(material => ({
      materialId: material.id,
      width: material.data.width,
      height: material.data.height
    }));
  const coverFinalSheet = {
    width: options.width * 2 + options.pages / 50,
    height: options.height
  };
  const coverLayout = autoLayout({
    largeSheets: coverLargeSheets,
    finalSheet: coverFinalSheet,
    printer,
    type: "default"
  });
  const coverBase = createOffsetComponent({
    layout: coverLayout,
    colors: options.cover.colors,
    frontAndBack: false,
    sheetsInGroup: 1
  });

  return {
    layouts: [interiorLayout, coverLayout],
    components: [
      ...groups.map((group, index) => {
        const base = createOffsetComponent({
          layout: interiorLayout,
          colors: options.interior.colors,
          frontAndBack: true,
          sheetsInGroup: group
        });
        return {
          ...base,
          name: `Coala #${index + 1}`
        };
      }),
      {
        ...coverBase,
        name: "Coperta",
        services: omitFalseValues({
          ...coverBase.services,
          lacuire: options.cover.varnish && {
            id: VARNISH_SERVICE,
            value: 1
          }
        })
      }
    ]
  };
}
