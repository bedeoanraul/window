import Vue from "vue";

import TimeAgo from "@/components/utils/TimeAgo";
import ResourceTable from "@/components/utils/ResourceTable";
import ResourceLoader from "@/components/utils/ResourceLoader";
import ResourceSelect from "@/components/utils/ResourceSelect";

const components = {
  TimeAgo,
  ResourceTable,
  ResourceLoader,
  ResourceSelect
};

Object.keys(components).forEach(name => {
  Vue.component(name, components[name]);
});
