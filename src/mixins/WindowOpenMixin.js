export default {
  methods: {
    windowOpen(...args) {
      const win = window.open(...args);
      if (!win) this.$alert("Browser-ul a blocat deschiderea ferestrei.");
    }
  }
};
