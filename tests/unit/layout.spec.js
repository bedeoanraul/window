import {
  getLayout,
  getPageGroups,
  getBestLayout,
  getBestLinearLayout
} from "@/recipes/utils";

const printer = { minWidth: 25, maxWidth: 50, minHeight: 35, maxHeight: 70 };
const largeSheets = [
  { name: "43x61", width: 43, height: 61 },
  { name: "61x86", width: 61, height: 86 },
  { name: "50x70", width: 50, height: 70 },
  { name: "70x100", width: 70, height: 100 }
];

test("getBestLinearLayout", () => {
  const layout = getBestLinearLayout({
    finalSheetSize: 15,
    largeSheetSize: 61,
    minPrinterSize: 25,
    maxPrinterSize: 50
  });

  expect(layout).toMatchObject({
    finalSheetsOnPrintSheet: 2,
    printSheetsOnLargeSheet: 2
  });
  expect(layout.coverage).toBeCloseTo(0.98);
});

test("getLayout", () => {
  const layout = getLayout({
    finalSheet: {
      width: 15,
      height: 21
    },
    largeSheet: {
      width: 61,
      height: 86
    },
    printer
  });

  expect(layout).toMatchObject({
    printSheetsOnLargeSheet: 2,
    finalSheetsOnPrintSheet: 8,
    finalSheetsOnLargeSheet: 16,
    printSheetLayout: { x: 4, y: 2 },
    largeSheetLayout: { x: 1, y: 2 }
  });
  expect(layout.coverage).toBeCloseTo(0.96);
});

test("getBestLayout 1", () => {
  const layout = getBestLayout({
    finalSheet: {
      width: 15,
      height: 21
    },
    largeSheets,
    printer
  });

  expect(layout).toMatchObject({
    printSheetsOnLargeSheet: 2,
    finalSheetsOnPrintSheet: 8,
    finalSheetsOnLargeSheet: 16,
    printSheetLayout: { x: 4, y: 2 },
    largeSheetLayout: { x: 1, y: 2 },
    largeSheet: { width: 61, height: 86 }
  });
  expect(layout.coverage).toBeCloseTo(0.96);
});

test("getBestLayout 2", () => {
  const layout = getBestLayout({
    finalSheet: {
      width: 21,
      height: 30
    },
    largeSheets,
    printer
  });

  expect(layout).toMatchObject({
    printSheetsOnLargeSheet: 1,
    finalSheetsOnPrintSheet: 4,
    finalSheetsOnLargeSheet: 4,
    printSheetLayout: { x: 2, y: 2 },
    largeSheetLayout: { x: 1, y: 1 },
    largeSheet: { width: 43, height: 61 }
  });
  expect(layout.coverage).toBeCloseTo(0.96);
});

test("getPageGroups", () => {
  const runs = [
    [2, 32, [2]],
    [32, 32, [32]],
    [34, 32, [32, 2]],
    [100, 32, [32, 32, 32, 4]]
  ];

  runs.forEach(run => {
    const [pages, pagesOnSheet, result] = run;
    expect(
      getPageGroups({
        pages,
        pagesOnSheet
      })
    ).toEqual(result);
  });
});
