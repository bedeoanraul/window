const admin = require("firebase-admin");
const functions = require("firebase-functions");

admin.initializeApp();

exports.deleteProjectFile = functions.firestore
  .document("projects/{projectId}/files/{fileId}")
  .onDelete(snapshot => {
    const path = snapshot.ref.path;
    const name = snapshot.data().name;

    return admin
      .storage()
      .bucket()
      .file(`${path}/${name}`)
      .delete();
  });
