# app

## TODO

- integrate firestore (x)
- integrate element ui (x)
- make deployment (x)
- learn cryptoscape (x)
- app title (x)
- simple inventory crud (x)
  - form validation (x)
  - route + dialogs (x)
  - subscribe & unsubscribe (x)
- replace store with \$root (x)
- cytoscape validation (x)
  - one cy.nodes().roots(), one cy.nodes().leaves() (x)
  - no circular references starting from root (x)
  - just one egde between two nodes (x)
- projects crud & details (x)
- tasks (x)
  - kanban (x)
  - priority on project (x)
  - refactor tasks models (x)
- add query param binding (x)
- change path to id (x)
- clients (x)
  - crud (x)
  - view projects button (x)
- suppliers (x)
  - crud (x)
  - view inventory button (x)
- CRUD taskCategories with colors (x)
- CRUD materialCategories (x)
- project file uploads (x)
  - delete percent on success (x)
  - rename percent to uploading (x)
  - add function to delete storage files (x)
  - view, delete file buttons (x)
- CRUD projectStates (x)
  - view projects button (x)
  - state filter on projects (x)
  * maybe these should be hard-coded
- rename project states to project stages (x)
  - add state to mark active projects (x)
  - stage = proposal, design, print, delivery (x)
  - state = active, canceled, paused, completed (x)
- remove stages (x)
  - too much hustle to manage them
  - some project will not go through all stage
- move labels in placeholder / option content (x)
- replace state with status (x)
  - Status: In Planificare, In Lucru, Finalizat
- add status in project details (x)
- on add is active by default (x)
- add priority in project details (x)
- firestore timestamps (x)
  - https://firebase.google.com/docs/reference/js/firebase.firestore.FieldValue#.serverTimestamp
  - createdAt (x)
  - updatedAt (x)
  - component time ago (x)
  - sort by name & timestamp (x)
- add index column (x)
- refactor inventory (x)
- extract db based selects (x)
- refactor filtering (x)
- touch updatedAt on project components (x)
- refactor diagram (x)
- extract canvas controls (x)
- refactor diagram css (x)
- save project changes (x)
- make right column (x)
- delete edges on node delete (x)
- edges should not care about false value (x)
- reverse dependencies directions (x)
- use uppercase for components in template (x)
- inventory minimum value (x)
- sort by lowest percent (x)
- sort createdAt and updatedAt (x)
- tasks view (x)
- task fields: alert, state, waitTime (x)
- inventory orders (x)
- inventory active orders filter (x)
- project description (x)
- FSC flags & filters (x)
- refactor filterResource (x)
- rename filter props & move to filters object (x)
- extract filters in component (x)
- activities limit/pagination (x)
- task asignees (x)
- task dialog (X)
- task.releaseAt (x)
- task.readyAt (x)
- project activities (x)
- project inventory (x)
- activities page (x)

next

- add tabs to project details (x)
- services table (x)
- settings table (x)
- rename inventory to materials (x)
- group components (x)
- project services (x)
- project pricing (x)
- add price/um to inventory
- create activities for each task action

materials => projectMaterials
services => projectServices

- dialog label width
- integrate tailwindcss (temporary)

* validate one root & one leaf (not needed)
* validate non-cyclical edges (not needed)

- camelCase all props
- remove orderBy where not needed
- delete related models in functions
- activity log
- project progres (completed tasks percentage)
- project inventory (array, flag to mark if removed from inventory)
- layout (.)
  - add dot-styles (x)
  - plugins can return string, array or objects
  - add basic flex, spacing, container
  - make a creator function that accepts (settings and plugins)
  - inventory layout
  - dialog layout
- diagram component (x)
  - firebase schema to cytoscape json (x)
  - sync changes from outside and from inside (x)

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
